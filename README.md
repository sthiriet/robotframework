
# GitLab CI template for Robot Framework

## Overview

This project implements a generic GitLab CI template for [Robot Framework](https://robotframework.org/).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/robotframework'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-robotframework.yml'
```

## Jobs

### `robotframework-lint` job

This job performs a **lint** analysis on Robot Framework files, using [robotframework-lint](https://github.com/boakley/robotframework-lint/).

It is bound to the `test` stage, and uses the following variable:

| Name                  | description                              | default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `ROBOT_LINT_DISABLED` | Disable linter                           | _none_ |

### `robotframework` job

This job performs [Robot Framework tests](https://robotframework.org/).

It is bound to the `acceptance` stage, and uses the following variable:

| Name                  | description                                                 | default value |
| --------------------- | ----------------------------------------------------------- | ------------- |
| `ROBOT_BASE_IMAGE`    | The Docker image used to run Robot Framework                | `ppodgorsek/robot-framework:latest` |
| `ROBOT_TESTS_DIR`     | Robot Framework's tests directory                           | `robot` |
| `ROBOT_THREADS`       | Number of threads to execute Robot Framework's tests (uses [Pabot](https://pabot.org/) if > `1`)  | `1` |
| `ROBOT_SCREEN_COLOUR_DEPTH` | Screen colour depth for X Window Virtual Framebuffer  | `24`   |
| `ROBOT_SCREEN_HEIGHT` | Screen height for X Window Virtual Framebuffer              | `1080` |
| `ROBOT_SCREEN_WIDTH`  | Screen width for X Window Virtual Framebuffer               | `1920` |
| `ROBOT_BROWSER`       | Browser to use (one of `firefox` or `chrome`)               | `firefox` |
| `ROBOT_OPTIONS`       | Robot Framework [additional options](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#all-command-line-options) | _none_ |
| `PABOT_OPTIONS`       | Pabot [additional options](https://github.com/mkorpela/pabot#command-line-options) (if `ROBOT_THREADS` > `1`)   | _none_ |
| `ACCEPTANCE_ALWAYS`   | Force to execute the test after each commit (even on development branches) | `false` |
| `REVIEW_ENABLED`      | Set to enable Robot Framework tests on review environments (_dynamic environments instantiated on development branches_) | _none_ (disabled) |

Notes:

* :warning: the default Docker image used here runs as non-`root` user. Therefore it is not able - like most other templates - to manage
custom certificate authorities (declared using either `$CUSTOM_CA_CERTS` or `$DEFAULT_CA_CERTS`). If you need it (need to call a server
with SSL certificate issued by a non-official certificate authority), then you'll have to build your own Docker image that either embeds
the required CA certificates, or that runs as `root` (thus the template will be able to install custom certificate authorities).
* The template runs [XVFB](https://www.x.org/releases/X11R7.6/doc/man/man1/Xvfb.1.xhtml) with `-screen` parameter
  `$ROBOT_SCREEN_WIDTHx$ROBOT_SCREEN_HEIGHTx$ROBOT_SCREEN_COLOUR_DEPTH` to run tests.
* Depending on the `ROBOT_THREADS` value, the template runs either [`robot`](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#all-command-line-options)
  or [`pabot`](https://pabot.org/) (if `ROBOT_THREADS` > 1).
* Tests are run automatically on `master` and `develop` branches, and manually on others (may be overridden setting the
  `REVIEW_ENABLED` variable).

### Robot Framework `{{ BASE_URL }}` auto evaluation

By default, the Robot Framework template tries to auto-evaluate the [`{{ BASE_URL }}` variable](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#variables)
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Robot Framework test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Robot Framework tests.

### How to manage Robot Framework tests in a separate GitLab repository

This template can obviously be used in an application repository that also embeds the Robot Framework test scripts.

But you may also decide to manage your acceptance tests in a **separate GitLab repository** (for instance if people developing those tests have
their own project cycle, separate from the application itself).

On the repository containing the Robot Framework tests, you should create a Docker image containing Robot Framework + your test scripts.

`Dockerfile` example:

```Dockerfile
FROM ppodgorsek/robot-framework:latest

LABEL name="My acceptance tests"
LABEL description="Robot Framework + tests in Docker"
LABEL url="https://gitlab.acme.host/my-project/rf-tests"
LABEL maintainer="john.dev@acme.com"

COPY robotframework/tests /opt/robotframework/tests

RUN pip3 install --no-cache-dir \
    <some robot framework library>
```

`.gitlab-ci.yml` example (build the Docker image using the Docker template):

```yml
include:
  # Include Docker template (to build the Docker image)
  - project: 'to-be-continuous/docker'
    ref: '<tag>'
    file: '/templates/gitlab-ci-docker.yml'
  # Include Robot Framework (to run robotframework-lint)
  - project: 'to-be-continuous/robotframework'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-robotframework.yml'

stages:
  - build
  - test
  - package-build
  - package-test
  - acceptance
  - publish
```

And lastly in the repository containing the application to test, you'll have to use the Robot Framework template but override
`ROBOT_BASE_IMAGE` with your own image built above.

```yml
include:
  # Include Robot Framework
  - project: 'to-be-continuous/robotframework'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-robotframework.yml'

variables:
  # use my own Robot Framework image (with embedded tests)
  ROBOT_IMAGE: '$CI_REGISTRYmy-project/rf-tests:master'
  ROBOT_THREADS: 4
  # Important: override the tests directory
  ROBOT_TESTS_DIR: "/opt/robotframework/tests"

stages:
  - build
  - test
  - deploy
  - acceptance
  - publish
  - production
```
